<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 05/08/2018
 * Time: 15:02
 */
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="lib/jquery-3.3.1.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <script src="lib/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <link href="css/jquery.toast.min.css" rel="stylesheet">
    <script src="js/jquery.toast.min.js"></script>

    <title><?= fctSettingItem('SITE_CONFIG', 'SITE_NAME') ?></title>

</head>